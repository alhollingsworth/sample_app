class FavouritesController < ApplicationController
before_action :logged_in_user

  def create
    @micropost = Micropost.find(params[:favourited_id])
    current_user.favourite(@micropost)
    redirect_to user_path (current_user)
  end

  def destroy
    @micropost = Favourite.find(params[:id]).favourited
    current_user.unfavourite(@micropost)
    redirect_to user_path (current_user)
  end  
    
end