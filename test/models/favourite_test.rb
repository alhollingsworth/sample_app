require 'test_helper'

class FavouriteTest < ActiveSupport::TestCase
  
  def setup
    @favourite = Favourite.new(favouriter_id: 1, favourited_id: 2)
  end
  
  test "should be valid" do
    assert @favourite.valid?
  end

  test "should require a favouriter_id" do
    @favourite.favouriter_id = nil
    assert_not @favourite.valid?
  end

  test "should require a favourited_id" do
    @favourite.favourited_id = nil
    assert_not @favourite.valid?
  end
end
